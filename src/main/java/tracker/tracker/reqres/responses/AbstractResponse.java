package tracker.tracker.reqres.responses;

abstract class AbstractResponse implements Response {

	int responsestatus;
	String errorMessage;
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public int getResponsestatus() {
		return responsestatus;
	}

	public void setResponsestatus(int responsestatus) {
		this.responsestatus = responsestatus;
	}
}
