package tracker.tracker.reqres.requests;

import tracker.tracker.reqres.responses.Response;

interface Request {
	Response process();

}