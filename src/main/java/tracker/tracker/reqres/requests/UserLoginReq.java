package tracker.tracker.reqres.requests;


import java.sql.SQLException;

import javax.xml.bind.annotation.XmlRootElement;

import tracker.tracker.dao.UserDao;
import tracker.tracker.model.ResponseStatus;
import tracker.tracker.model.User;
import tracker.tracker.reqres.responses.UserLoginResponse;

@XmlRootElement
public class UserLoginReq extends AbstractRequest {
	User user;
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public UserLoginResponse process() {
		UserLoginResponse response = new UserLoginResponse();
		UserDao dao = new UserDao();
		if(user != null) {
			System.out.print("UserLoginReq process started..........");
				User dbuser = null;
				try {
					dbuser = dao.fetchUserData(user.getUsername());
					if(dbuser != null ) {
						if(dbuser.getPassword().equals(user.getPassword())) {
							response.setResponsestatus(ResponseStatus.SUCCESS.getId());
							response.setErrorMessage(ResponseStatus.SUCCESS.getMessage());;
						}else {
							response.setResponsestatus(ResponseStatus.FAILURE.getId());
							response.setErrorMessage("password not same");
						}
					}else {
						response.setResponsestatus(ResponseStatus.FAILURE.getId());
						response.setErrorMessage(ResponseStatus.FAILURE.getMessage());;
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
		}
			return response;
	
		
	
}
}
