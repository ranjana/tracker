package tracker.tracker.reqres.requests;

import java.sql.SQLException;

import javax.xml.bind.annotation.XmlRootElement;

import tracker.tracker.dao.UserDao;
import tracker.tracker.model.DataBaseErrorType;
import tracker.tracker.model.ResponseStatus;
import tracker.tracker.model.UserProfile;
import tracker.tracker.reqres.responses.UserRegistrationResponse;

@XmlRootElement
public class UserRegistrationRequest extends AbstractRequest {
	UserProfile profile;
	public UserProfile getProfile() {
		return profile;
	}

	public void setProfile(UserProfile profile) {
		this.profile = profile;
	}

	@Override
	public UserRegistrationResponse process() {
		UserRegistrationResponse response = new UserRegistrationResponse();
		UserDao dao = new UserDao();
		if(profile != null) {
			System.out.print("inside UserRegistrationRequest process");
			try {
				DataBaseErrorType dbErrorType =	dao.insert(profile.getUser());
				if(dbErrorType == DataBaseErrorType.DUPLICATE_USERNAME) {
					response.setResponsestatus(ResponseStatus.FAILURE.getId());
					return response;
				}
				dao.insertAddress(profile.getAddres(),profile.getUser().getUsername());
				response.setResponsestatus(ResponseStatus.SUCCESS.getId());
				return response;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		response.setResponsestatus(ResponseStatus.FAILURE.getId());
		return response;
	}
}