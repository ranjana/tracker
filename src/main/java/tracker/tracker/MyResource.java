
package tracker.tracker;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import tracker.tracker.repository.UserRepository;
import tracker.tracker.reqres.requests.UserLoginReq;
import tracker.tracker.reqres.requests.UserRegistrationRequest;
import tracker.tracker.reqres.responses.UserLoginResponse;
import tracker.tracker.reqres.responses.UserRegistrationResponse;

/**
 * Example resource class hosted at the URI path "/myresource"
 */
@Path("/myresource")
public class MyResource {

	/**
	 * Method processing HTTP GET requests, producing "text/plain" MIME media type.
	 * 
	 * @return String that will be send back as a response of type "text/plain".
	 */
	@GET
	@Produces("text/plain")
	public String getIt() {
		return "Hi there!";
	}

	@POST
	@Path("register")
	@Produces(MediaType.APPLICATION_JSON)
	public UserRegistrationResponse register(UserRegistrationRequest req) {
		System.out.println("inside registration ..........");
		return UserRepository.register(req);

	}
	
	@POST
	@Path("login")
	@Produces(MediaType.APPLICATION_JSON)
	public UserLoginResponse test(UserLoginReq user) {
		System.out.println("inside login resources....");
		return UserRepository.login(user);

	}
}
