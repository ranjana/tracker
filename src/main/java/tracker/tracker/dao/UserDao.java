package tracker.tracker.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import tracker.tracker.databases.DataBaseConnection;
import tracker.tracker.model.Address;
import tracker.tracker.model.DataBaseErrorType;
import tracker.tracker.model.User;

public class UserDao implements UserInterface {
	
	static Connection con = DataBaseConnection.getConnection(); 
	@Override
	public DataBaseErrorType insert(User user) throws SQLException {
		createTable();
		String sql = "INSERT INTO users VALUES (?,?,?,?,?)";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, user.getId());
			ps.setString(2,user.getUsername());
			ps.setString(3, user.getFirstname());
			ps.setString(4,user.getSurname());
			ps.setString(5,user.getPassword());
			ps.executeUpdate();
			System.out.println("successfully inserted");
			return DataBaseErrorType.SUCCESS;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return DataBaseErrorType.DUPLICATE_USERNAME;
	}
	@Override
	public void createTable() throws SQLException {
		  String sqlCreate = "CREATE TABLE IF NOT EXISTS " +"users("
		  +" id int(11) NOT NULL auto_increment," 
          + " username VARCHAR(50),"
          + " firstname VARCHAR(50),"
          + " surname VARCHAR(50),"
          + " password VARCHAR(50) NOT NULL,"
          +" UNIQUE (username),"
          +" PRIMARY KEY  (id))";
		  Statement stmt = con.createStatement();
		  stmt.execute(sqlCreate);
	}
	@Override
	public void createAddressTable() throws SQLException {
		  String sqlCreate = "CREATE TABLE IF NOT EXISTS " +"addressUser("
				  +" id int(11) NOT NULL auto_increment," 
		          + " username VARCHAR(100),"
		          + " line1 VARCHAR(100),"
		          + " line2 VARCHAR(100),"
		          + " city VARCHAR(50),"
		          + " pin VARCHAR(10),"
		          + " district VARCHAR(20),"
		          + " state VARCHAR(50),"
		          + " country VARCHAR(50),"
		          +" PRIMARY KEY  (id))";
				  Statement stmt = con.createStatement();
				  stmt.execute(sqlCreate);
	}
	@Override
	public String insertAddress(Address address,String username) throws SQLException {
		// TODO Auto-generated method stub
		createAddressTable();
		String sql = "INSERT INTO addressUser VALUES (?,?,?,?,?,?,?,?,?)";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, address.getId());
			ps.setString(2,username);
			ps.setString(3,address.getLine1());
			ps.setString(4, address.getLine2());
			ps.setString(5,address.getCity());
			ps.setString(6,address.getPin());
			ps.setString(7,address.getDistrict());
			ps.setString(8, address.getState());
			ps.setString(9,address.getCountry());
			ps.executeUpdate();
			return "address successfully inserted";
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "insertion faild may be there is dublicate username";
	}
	@Override
	public User fetchUserData(String username) throws SQLException {
		// TODO Auto-generated method stub
		String sql = "select * from users where username = "+"\""+username+"\"";
		User user = new User();
		  Statement stmt = con.createStatement();
		  ResultSet rs = stmt.executeQuery(sql);
		  while(rs.next()) {
			  user.setId(rs.getInt("id"));
			  user.setUsername(rs.getString("username"));
			  user.setPassword(rs.getString("password"));
			  user.setFirstname(rs.getString("firstname"));
			  user.setSurname(rs.getString("surname"));
		  }
		  rs.close();
		return user;
	}

}
