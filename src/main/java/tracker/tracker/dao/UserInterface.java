package tracker.tracker.dao;
import java.sql.SQLException;

import tracker.tracker.model.Address;
import tracker.tracker.model.DataBaseErrorType;
import tracker.tracker.model.User;
import tracker.tracker.model.UserProfile;

public interface UserInterface {
	public void createTable() throws SQLException; 
	public DataBaseErrorType insert(User user) throws SQLException; 
	public User fetchUserData(String username)throws SQLException; ;
	public void createAddressTable() throws SQLException; 
	public String insertAddress(Address address,String username) throws SQLException; 


}
