package tracker.tracker.repository;

import tracker.tracker.reqres.requests.UserLoginReq;
import tracker.tracker.reqres.requests.UserRegistrationRequest;
import tracker.tracker.reqres.responses.UserLoginResponse;
import tracker.tracker.reqres.responses.UserRegistrationResponse;

public class UserRepository {

	public static UserRegistrationResponse register(UserRegistrationRequest req) {
		if(req != null) {
		return req.process();
		}
		return null;
	}
	
	public static UserLoginResponse login(UserLoginReq req) {
		if(req != null) {
		return req.process();
		}
		return null;
	}
}
