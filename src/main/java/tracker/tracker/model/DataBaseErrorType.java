package tracker.tracker.model;

public enum DataBaseErrorType {
	SUCCESS(1,"success"),DUPLICATE_USERNAME(2,"dublicate username");

	int id;
	String message;
	DataBaseErrorType(int id, String message) {
		this.id = id;
		this.message = message;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
