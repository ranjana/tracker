package tracker.tracker.model;

public enum ResponseStatus {

	SUCCESS(1,"success"),FAILURE(2,"request not valid");

	int id;
	String message;
	ResponseStatus(int id, String message) {
		this.id = id;
		this.message = message;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

}
