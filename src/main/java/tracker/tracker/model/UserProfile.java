package tracker.tracker.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserProfile {
	User user;
	Address addres;
	public Address getAddres() {
		return addres;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public void setAddres(Address addres) {
		this.addres = addres;
	}
	public User getUser() {
		// TODO Auto-generated method stub
		return user;
	}
}
