package tracker.tracker.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class User {
	int id;
	String firstname; 
	String surname;
	String username; 
	String password;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		System.out.println(firstname);
		this.firstname = firstname;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		System.out.println(surname);
		this.surname = surname;
	}
	@Override
	public String toString() {
		System.out.println("toString");

		return "User [name=" + firstname + ", surname=" + surname + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	} 	
	
}

