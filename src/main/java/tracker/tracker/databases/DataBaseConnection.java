package tracker.tracker.databases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBaseConnection {

	private static Connection con = null; 
	  
    static
    { 
    	String url = "jdbc:mysql://localhost:3306/user_Information?allowPublicKeyRetrieval=true&useSSL=false";
		String username = "root";
		String pass = "";
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection(url, username, pass); 
        } 
        catch (ClassNotFoundException | SQLException e) { 
            e.printStackTrace(); 
        } 
    } 
    public static Connection getConnection() 
    { 
        return con; 
    } 
}
